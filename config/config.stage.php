<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Staging config overrides & db credentials
 * 
 * Our database credentials and any environment-specific overrides
 * 
 * @package    Focus Lab Master Config
 * @version    1.1.1
 * @author     Focus Lab, LLC <dev@focuslabllc.com>
 */

$env_db['hostname'] = '70.32.105.29';
$env_db['username'] = 'rubiwebdb';
$env_db['password'] = 'eFut7FeQv8vCqh';
$env_db['database'] = 'rubiweb';

/**
 * Example of multiple database usage
 */
// $env_db['expressionengine']['hostname'] = '';
// $env_db['expressionengine']['username'] = '';
// $env_db['expressionengine']['password'] = '';
// $env_db['expressionengine']['database'] = '';

// $env_db['another_db']['hostname'] = '';
// $env_db['another_db']['username'] = '';
// $env_db['another_db']['password'] = '';
// $env_db['another_db']['database'] = '';
// $env_db['another_db']['dbprefix'] = '';
// $env_db['another_db']['swap_pre'] = '';
// $env_db['another_db']['dbdriver'] = 'mysql';
// $env_db['another_db']['pconnect'] = FALSE;
// $env_db['another_db']['db_debug'] = TRUE;
// $env_db['another_db']['cache_on'] = FALSE;
// $env_db['another_db']['autoinit'] = FALSE;
// $env_db['another_db']['char_set'] = 'utf8';
// $env_db['another_db']['dbcollat'] = 'utf8_general_ci';
// $env_db['another_db']['cachedir'] = '';

// Path to Stash Templates
// $env_config['stash_file_basepath'] = '';


$env_config['webmaster_email'] = 'noreply@mtrubidouxsda.org';
$env_global['global:environment'] = 'staging';

$config['newrelic_app_name'] = 'Mt. Rubidoux Staging';


/* End of file config.stage.php */
/* Location: ./config/config.stage.php */