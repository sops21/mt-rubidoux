set :application_path, "staging"
set :deploy_to, "#{server_root}/#{application_path}"
set :rails_env, "staging"
set :branch, "staging"

server "70.32.105.29", :app, :web, :db, :primary => true

set :document_root, "httpdocs"
set :ee_system, "system"

after("deploy", "deploy:secure_staging")
# after("deploy","deploy:create_cache", "deploy:set_permissions", "deploy:create_symlinks", "deploy:set_permissions") 