set :application, "mtrubidoux"
set :stages, %w(development staging production)
set :default_stage, 'staging' 
require 'capistrano/ext/multistage'

set :repository,  "git@bitbucket.org:sops21/mt-rubidoux.git"
set :server_root, "/var/www/vhosts/mtrubidouxsda.org"

set :deploy_via, :remote_cache
default_run_options[:pty] = true 

set :ssh_options, { :keys => ['~/.ssh/id_rsa'], :forward_agent => true, :user => "root" }
set :scm, :git
set :use_sudo, false
set :keep_releases, 5
set :copy_strategy, :checkout
set :copy_exclude, [".git", ".gitignore"]
set :keep_releases, 5
set :use_sudo, false
set :copy_compression, :bz2

# server "70.32.105.29", :app, :web, :db, :primary => true
# set :deploy_to, "/var/www/vhosts/mtrubidouxsda.org/#{application}"


# Deployment process
after("deploy:setup", "deploy:create_shared")
after("deploy:update", "deploy:cleanup")
after("deploy","deploy:create_cache", "deploy:set_permissions", "deploy:create_symlinks", "deploy:set_permissions")

# Custom deployment tasks
namespace :deploy do
  
  desc "EE: Create shared directories and set permissions after initial setup"
  task :create_shared, :roles => [:app] do
    # create upload directories
    run "mkdir -p #{server_root}/#{shared_dir}/static"
    run "mkdir -p #{server_root}/#{shared_dir}/images"
    run "mkdir -p #{server_root}/#{shared_dir}/images/made"
    run "mkdir -p #{server_root}/#{shared_dir}/images/remote"
    run "mkdir -p #{server_root}/#{shared_dir}/images/avatars/uploads"
    run "mkdir -p #{server_root}/#{shared_dir}/images/captchas"
    run "mkdir -p #{server_root}/#{shared_dir}/images/member_photos"
    run "mkdir -p #{server_root}/#{shared_dir}/images/pm_attachments"
    run "mkdir -p #{server_root}/#{shared_dir}/images/signature_attachments"
    run "mkdir -p #{server_root}/#{shared_dir}/images/uploads"
    run "mkdir -p #{server_root}/#{shared_dir}/logs"
    
    # Required for Brilliant Retail
    run "mkdir -p #{server_root}/#{shared_dir}/store"
    run "mkdir -p #{server_root}/#{shared_dir}/store/attachments"
    run "mkdir -p #{server_root}/#{shared_dir}/store/download"
    run "mkdir -p #{server_root}/#{shared_dir}/store/file"
    run "mkdir -p #{server_root}/#{shared_dir}/store/images"
    run "mkdir -p #{server_root}/#{shared_dir}/store/products"
    run "mkdir -p #{server_root}/#{shared_dir}/store/products/thumb"
    
    # Custom Upload directories
    run "mkdir -p #{server_root}/#{shared_dir}/images/uploads/speakers_staff"
    run "mkdir -p #{server_root}/#{shared_dir}/images/uploads/media"
    run "mkdir -p #{server_root}/#{shared_dir}/images/uploads/giving_projects"
    run "mkdir -p #{server_root}/#{shared_dir}/images/uploads/media/downloads"
    run "mkdir -p #{server_root}/#{shared_dir}/images/uploads/events"
    run "mkdir -p #{server_root}/#{shared_dir}/images/uploads/downloads"
    run "mkdir -p #{server_root}/#{shared_dir}/images/uploads/home"
    run "mkdir -p #{server_root}/#{shared_dir}/images/uploads/page-header"
  end

  desc "This is here to override the original :restart"
  task :restart, :roles => :app do
    # do nothing but override the default
  end 
   
  desc "EE: Create the cache folder"
  task :create_cache, :roles => [:app] do
    run "mkdir -p #{current_release}/#{ee_system}/expressionengine/cache"
    run "mkdir -p #{current_release}/#{document_root}/store/cache"
  end

  desc "Set the correct permissions for the config files and cache folder"
  task :set_permissions, :roles => :app do
    run "chmod 666 #{current_release}/#{document_root}/index.php"
    run "chmod 777 #{current_release}/#{document_root}/.htaccess"
    run "chmod 777 #{current_release}/#{ee_system}/expressionengine/cache"
    run "chmod 666 #{current_release}/#{ee_system}/expressionengine/config/config.php"
    run "chmod 666 #{current_release}/#{ee_system}/expressionengine/config/database.php"
    run "chmod 777 #{current_release}/#{document_root}/store/cache"
    
    run "chmod 777 #{current_release}/templates"
    run "chmod 777 #{server_root}/#{shared_dir}/logs"
    
    run "chmod 777 #{server_root}/#{shared_dir}/static"
    run "chmod 777 #{server_root}/#{shared_dir}/images"
    run "chmod 777 #{server_root}/#{shared_dir}/images/made"
    run "chmod 777 #{server_root}/#{shared_dir}/images/remote"
    run "chmod 777 #{server_root}/#{shared_dir}/images/avatars/uploads"
    run "chmod 777 #{server_root}/#{shared_dir}/images/captchas"
    run "chmod 777 #{server_root}/#{shared_dir}/images/member_photos"
    run "chmod 777 #{server_root}/#{shared_dir}/images/pm_attachments"
    run "chmod 777 #{server_root}/#{shared_dir}/images/signature_attachments"
    
    # -R tag handles all subfolders
    run "chmod -R 777 #{server_root}/#{shared_dir}/images/uploads"
    
    # Required for Brilliant Retail
    # -R tag handles all subfolders
    run "chmod -R 777 #{server_root}/#{shared_dir}/store"
  end
  
  desc "EE: Create symlinks to shared data (eg. config files and uploaded images)"
  task :create_symlinks, :roles => [:app] do
    # standard image upload directories
    run "ln -nfs #{server_root}/#{shared_dir}/static #{current_release}/#{document_root}/static"
    run "ln -nfs #{server_root}/#{shared_dir}/images/made #{current_release}/#{document_root}/images/made"
    run "ln -nfs #{server_root}/#{shared_dir}/images/remote #{current_release}/#{document_root}/images/remote"
    run "ln -nfs #{server_root}/#{shared_dir}/images/avatars/uploads #{current_release}/#{document_root}/images/avatars/uploads"
    run "ln -nfs #{server_root}/#{shared_dir}/images/captchas #{current_release}/#{document_root}/images/captchas"
    run "ln -nfs #{server_root}/#{shared_dir}/images/member_photos #{current_release}/#{document_root}/images/member_photos"
    run "ln -nfs #{server_root}/#{shared_dir}/images/pm_attachments #{current_release}/#{document_root}/images/pm_attachments"
    run "ln -nfs #{server_root}/#{shared_dir}/images/signature_attachments #{current_release}/#{document_root}/images/signature_attachments"
    
    # also handles subfolders of uploads
    run "ln -s #{server_root}/#{shared_dir}/images/uploads #{current_release}/#{document_root}/images/uploads"
    
    # also handles subfolders of store
    run "ln -s #{server_root}/#{shared_dir}/store #{current_release}/#{document_root}/store"
  end
  
  desc "Rename htpasswd and robots file to secure staging"
  task :secure_staging do
    run "mv #{current_release}/#{document_root}/dev.htpasswd #{current_release}/#{document_root}/.htpasswd"
    run "mv #{current_release}/#{document_root}/dev.robots.txt #{current_release}/#{document_root}/robots.txt"
  end

end