<?php
if (! defined('MX_GETID3_KEY'))
{
	define('MX_GETID3_NAME', 'MX GetID3');
	define('MX_GETID3_VER',  '1.0.0');
	define('MX_GETID3_KEY', 'mx_getid3');
	define('MX_GETID3_AUTHOR',  'Max Lazar');
	define('MX_GETID3_DOCS',  'http://www.eec.ms/add-on/mx-getid3');
	define('MX_GETID3_DESC',  'getID3() extracts useful information from MP3s & other multimedia file formats.');

}

/*
 * NSM Addon Updater
$config['name'] = MX_GETID3_NAME;
$config['version'] = MX_GETID3_VER;
$config['nsm_addon_updater']['versions_xml'] = 'http://www.eec.ms/versions/mx-getid3';
*/